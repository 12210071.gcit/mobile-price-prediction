from flask import Flask,render_template,request
import pandas as pd
import pickle

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/predict')
def predict():
    return render_template('predict.html')

@app.route('/mobilepredict', methods=['POST'])
def autopredict():
    if request.method == 'POST':
        ram = float(request.form.get('ram'))
        rom = float(request.form.get('rom'))
        dis = float(request.form.get('display'))
        batt = float(request.form.get('bp'))
        frontcam = float(request.form.get('camera'))
        bcam1 = float(request.form.get('camera1'))
        bcam2 = float(request.form.get('camera2'))
        bcam3 = float(request.form.get('camera3'))
        bcam4 = float(request.form.get('camera4'))
        bcam5 = float(request.form.get('camera5'))
        
        model = pickle.load(open('model.pkl', 'rb'), encoding='utf-8')
        data = [[ram,rom,dis,batt,frontcam,bcam1,bcam2,bcam3,bcam4,bcam5]]

        expected_columns = ['RAM', 'ROM', 'display', 'battery power', 'Front camera', 'camera1', 'camera2', 'camera3', 'camera4', 'camera5']
        x_sample = pd.DataFrame(data, columns=expected_columns)
        pred = model.best_estimator_.predict(x_sample)
        rounded_pred = int(round(pred[0]))
    
        return render_template('predict.html', pred=rounded_pred)




if __name__ == '__main__':
    app.run(debug=True)
